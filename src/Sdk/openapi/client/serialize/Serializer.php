<?php

namespace JyEleRetail\Sdk\openapi\client\serialize;

interface Serializer
{
    public function supportedContentType();
    
    public function serialize($serializer);
}

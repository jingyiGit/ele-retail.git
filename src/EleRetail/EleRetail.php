<?php

namespace JyEleRetail\EleRetail;

use JyEleRetail\Sdk\Facade;

class EleRetail
{
    use Shop;
    use Good;
    use Order;
    
    protected $config = [];
    protected $error = null;
    
    public function __construct($config = null)
    {
        $this->config = $config;
    }
    
    private function getApp()
    {
        $app = new Facade ();
        $app->setAppKey($this->config['appid']);
        $app->setSecKey($this->config['secret']);
        $app->setServerHost("https://api-be.ele.me");
        return $app;
    }
    
    private function handleReturn($res)
    {
        if ($res['errno'] == 0) {
            $this->error = null;
            return $res['body']['data'] ?: $res['body'];
        }
        $this->setError($res);
        return false;
    }
    
    public function getError()
    {
        return $this->error;
    }
    
    private function setError($error)
    {
        $this->error = $error;
        return false;
    }
}
